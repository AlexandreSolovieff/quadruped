import argparse
import math
import pybullet as p
from time import sleep
from scipy.interpolate import interp1d

dt = 0.01

l0 = 4.0
l1 = 4.5
l2 = 6.5
l3 = 8.7


def init():
    """Initialise le simulateur
    
    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    """Definis les angles cibles pour les moteurs du robot
    
    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

def stable_pos():
    joints = [0]*12
    pos = [0, math.pi/4.0, 3*math.pi/4.0]

    joints[0:3], joints[3:6], joints[6:9], joints[9:12] = pos, pos, pos, pos

    return joints

def inverse_control(x, y, z):
    theta0 = math.atan2(y, x)
    A = [l1 * math.cos(theta0), l1 * math.sin(theta0), 0] 
    AM = math.sqrt((x - A[0])**2 + (y - A[1])**2 + (z - A[2])**2)

    temp = (l2**2 + l3**2 - AM**2)/(2.0*l2*l3)
    if (temp > 1):
        temp = 1
    elif (temp < -1):
        temp = -1
    theta2 = math.acos(temp)

    temp = (AM**2 + l2**2 -l3**2) / (2.0*l2*AM)
    if (temp > 1):
        temp = 1
    elif (temp < -1):
        temp = -1
    alpha = math.acos(temp)

    temp = z/AM
    if (temp > 1):
        temp = 1
    elif (temp < -1):
        temp = -1
    beta = math.asin(temp)

    theta1 = math.pi - (alpha + beta)

    if (theta0 > math.pi/2.0): theta0 = math.pi/2.0
    if (theta0 < -math.pi/2.0): theta0 = -math.pi/2.0
    if (theta1 > 11*math.pi/6.0): theta1 = 11*math.pi/6.0
    if (theta1 < math.pi/6.0): theta1 = math.pi/6.0
    if (theta2 > 11*math.pi/6.0): theta2 = 11*math.pi/6.0
    if (theta2 < math.pi/6.0): theta2 = math.pi/6.0

    return (theta0, math.pi-theta1, math.pi-theta2)

def leg_angle(leg_nb):
    """Angle de la patte correspondante par rapport au repere du corps du robot
    
    Arguments:
        leg_nb {int} -- Numero de la patte (entre 0 et 3)
    
    Returns:
        float -- L'angle de la patte par rapport au repere du corps du robot
    """
    if (leg_nb == 0):
        angle = math.pi/4.0   
    elif (leg_nb == 1):
        angle = math.pi/4.0 + math.pi/2.0
    elif (leg_nb == 2):
        angle = -math.pi/4.0 - math.pi/2.0
    else:
        angle = -math.pi/4.0
    return angle

def leg_ref(leg_nb, x_ref_robot, y_ref_robot, z_ref_robot):
    """Coordonnees dans le repere de la patte correspondante
    
    Arguments:
        leg_nb      {int}   -- Numero de la patte (entre 0 et 3)
        x_ref_robot {float} -- Coordonnee x dans le repere du robot
        y_ref_robot {float} -- Coordonnee y dans le repere du robot
        z_ref_robot {float} -- Coordonnee z dans le repere du robot
    
    Returns:
        float -- Coordonnee x dans le repere de la patte
        float -- Coordonnee y dans le repere de la patte
        float -- Coordonnee z dans le repere de la patte
    """
    z = z_ref_robot
    
    angle = leg_angle(leg_nb)

    x =  math.cos(angle) * (x_ref_robot - math.cos(angle)*l0) + math.sin(angle) * (y_ref_robot - math.sin(angle)*l0)
    y = -math.sin(angle) * (x_ref_robot - math.cos(angle)*l0) + math.cos(angle) * (y_ref_robot - math.sin(angle)*l0)
    return (x, y, z)

def robot_ref(leg_nb, theta0, theta1, theta2):
    """Coordonnees dans le repere du robot
    
    Arguments:
        leg_nb {int}   -- Numero de la patte (entre 0 et 3)
        theta0 {float} -- Angle de rotation du premier moteur
        theta1 {float} -- Angle de rotation du second moteur
        theta2 {float} -- Angle de rotation du troisieme moteur
    
    Returns:
        float -- Coordonnee x dans le repere du robot
        float -- Coordonnee y dans le repere du robot
        float -- Coordonnee z dans le repere du robot
    """
    angle = leg_angle(leg_nb)

    d = l1 + math.cos(theta1)*l2 + math.cos(theta1-theta2)*l3

    x = math.cos(angle + theta0)*d + math.cos(angle)*l0
    y = math.sin(angle + theta0)*d + math.sin(angle)*l0
    z = math.sin(theta1)*l2 + math.sin(theta1 - theta2)*l3 
    return (x, y, z)

def demo(t, amplitude):
    """Demonstration de mouvement (fait osciller une patte)
    
    Arguments:
        t {float} -- Temps ecoule depuis le debut de la simulation
    
    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints

def leg_ik(x, y ,z, t):
    """Deplace le bout d'une patte aux coordonnees apres 5 sec
    
    Arguments:
        x {float} -- coordonnee x d'arrivee
        y {float} -- coordonnee y d'arrivee
        z {float} -- coordonnee z d'arrivee
        t {float} -- Temps ecoule depuis le debut de la simulation
    
    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
    """
    joints = stable_pos()
    joints[0:3] = inverse_control(x, y, z)
    return joints

def robot_ik(x, y, z, t):
    joints = stable_pos()
    (leg_x, leg_y, leg_z) = robot_ref(1, joints[0], joints[1], joints[2])
    leg_x -= x
    leg_y -= y
    leg_z -= z
    (new_x, new_y, new_z) = leg_ref(1, leg_x, leg_y, leg_z)
    joints[0:3] = inverse_control(new_x, new_y, new_z)

    (leg_x, leg_y, leg_z) = robot_ref(2, joints[3], joints[4], joints[5])
    leg_x -= x
    leg_y -= y
    leg_z -= z
    (new_x, new_y, new_z) = leg_ref(2, leg_x, leg_y, leg_z)
    joints[3:6] = inverse_control(new_x, new_y, new_z)
    
    (leg_x, leg_y, leg_z) = robot_ref(3, joints[6], joints[7], joints[8])
    leg_x -= x
    leg_y -= y
    leg_z -= z
    (new_x, new_y, new_z) = leg_ref(3, leg_x, leg_y, leg_z)
    joints[6:9] = inverse_control(new_x, new_y, new_z)
    
    (leg_x, leg_y, leg_z) = robot_ref(0, joints[9], joints[10], joints[11])
    leg_x -= x
    leg_y -= y
    leg_z -= z
    (new_x, new_y, new_z) = leg_ref(0, leg_x, leg_y, leg_z)
    joints[9:12] = inverse_control(new_x, new_y, new_z)
    return joints

phase = 0
t_start = 0
fn_interpolate0 = []
fn_interpolate1 = []
fn_interpolate2 = []
fn_interpolate3 = []

joints_robot = stable_pos()
body = False
def walk(x, y, r, t):
    global phase
    global t_start
    global fn_interpolate0
    global fn_interpolate1
    global fn_interpolate2
    global fn_interpolate3
    global joints_robot
    global body
    
    if (x == 0 and y == 0): return joints_robot

    leg0R = robot_ref(0, joints_robot[9], joints_robot[10], joints_robot[11])
    leg1R = robot_ref(1, joints_robot[0], joints_robot[1], joints_robot[2])
    leg2R = robot_ref(2, joints_robot[3], joints_robot[4], joints_robot[5])  
    leg3R = robot_ref(3, joints_robot[6], joints_robot[7], joints_robot[8])
    if (body):
        joints_robot = robot_ik(x, y, 0, t)
        joints = joints_robot
        body = False
        return joints

    if (phase == 0):
        t_start = t

        fn_interpolate0[:] = []
        fn_interpolate0.append(interp1d([t, t+0.5, t+1.0], [leg0R[0], (leg0R[0]+x)/2.0, leg0R[0]+x], kind='quadratic'))
        fn_interpolate0.append(interp1d([t, t+0.5, t+1.0], [leg0R[1], (leg0R[1]+y)/2.0, leg0R[1]+y], kind='quadratic'))
        fn_interpolate0.append(interp1d([t, t+0.5, t+1.0], [leg0R[2], leg0R[2]+2.0, leg0R[2]], kind='quadratic'))

        fn_interpolate2[:] = []
        fn_interpolate2.append(interp1d([t, t+0.5, t+1.0], [leg2R[0], (leg2R[0]+x)/2.0, leg2R[0]+x], kind='quadratic'))
        fn_interpolate2.append(interp1d([t, t+0.5, t+1.0], [leg2R[1], (leg2R[1]+y)/2.0, leg2R[1]+y], kind='quadratic'))
        fn_interpolate2.append(interp1d([t, t+0.5, t+1.0], [leg2R[2], leg2R[2]+2.0, leg2R[2]], kind='quadratic'))

        leg0_x, leg0_y, leg0_z = fn_interpolate0[0](t), fn_interpolate0[1](t), fn_interpolate0[2](t)
        leg2_x, leg2_y, leg2_z = fn_interpolate2[0](t), fn_interpolate2[1](t), fn_interpolate2[2](t)

        new0_x, new0_y, new0_z = leg_ref(0, leg0_x, leg0_y, leg0_z)
        new2_x, new2_y, new2_z = leg_ref(2, leg2_x, leg2_y, leg2_z)

        joints_robot[9:12] = inverse_control(new0_x, new0_y, new0_z)
        joints_robot[3:6] = inverse_control(new2_x, new2_y, new2_z)

        phase = 1
    elif (phase == 1):
        if (t > t_start+1.0): t = t_start+1.0
        
        leg0_x, leg0_y, leg0_z = fn_interpolate0[0](t), fn_interpolate0[1](t), fn_interpolate0[2](t)
        leg2_x, leg2_y, leg2_z = fn_interpolate2[0](t), fn_interpolate2[1](t), fn_interpolate2[2](t)

        new0_x, new0_y, new0_z = leg_ref(0, leg0_x, leg0_y, leg0_z)
        new2_x, new2_y, new2_z = leg_ref(2, leg2_x, leg2_y, leg2_z)

        joints_robot[9:12] = inverse_control(new0_x, new0_y, new0_z)
        joints_robot[3:6] = inverse_control(new2_x, new2_y, new2_z)

        if (t == t_start+1.0):
            phase = 2
    elif (phase == 2):
        t_start = t
        fn_interpolate1[:] = []
        fn_interpolate1.append(interp1d([t, t+0.5, t+1.0], [leg1R[0], (leg1R[0]+x)/2.0, leg1R[0]+x], kind='quadratic'))
        fn_interpolate1.append(interp1d([t, t+0.5, t+1.0], [leg1R[1], (leg1R[1]+y)/2.0, leg1R[1]+y], kind='quadratic'))
        fn_interpolate1.append(interp1d([t, t+0.5, t+1.0], [leg1R[2], leg1R[2]+2.0, leg1R[2]], kind='quadratic'))

        fn_interpolate3[:] = []
        fn_interpolate3.append(interp1d([t, t+0.5, t+1.0], [leg3R[0], (leg3R[0]+x)/2.0, leg3R[0]+x], kind='quadratic'))
        fn_interpolate3.append(interp1d([t, t+0.5, t+1.0], [leg3R[1], (leg3R[1]+y)/2.0, leg3R[1]+y], kind='quadratic'))
        fn_interpolate3.append(interp1d([t, t+0.5, t+1.0], [leg3R[2], leg3R[2]+2.0, leg3R[2]], kind='quadratic'))

        leg1_x, leg1_y, leg1_z = fn_interpolate1[0](t), fn_interpolate1[1](t), fn_interpolate1[2](t)
        leg3_x, leg3_y, leg3_z = fn_interpolate3[0](t), fn_interpolate3[1](t), fn_interpolate3[2](t)

        new1_x, new1_y, new1_z = leg_ref(1, leg1_x, leg1_y, leg1_z)
        new3_x, new3_y, new3_z = leg_ref(3, leg3_x, leg3_y, leg3_z)

        joints_robot[0:3] = inverse_control(new1_x, new1_y, new1_z)
        joints_robot[6:9] = inverse_control(new3_x, new3_y, new3_z)

        phase = 3
    else:
        if (t > t_start+1.0): t = t_start+1.0

        leg1_x, leg1_y, leg1_z = fn_interpolate1[0](t), fn_interpolate1[1](t), fn_interpolate1[2](t)
        leg3_x, leg3_y, leg3_z = fn_interpolate3[0](t), fn_interpolate3[1](t), fn_interpolate3[2](t)

        new1_x, new1_y, new1_z = leg_ref(1, leg1_x, leg1_y, leg1_z)
        new3_x, new3_y, new3_z = leg_ref(3, leg3_x, leg3_y, leg3_z)

        joints_robot[0:3] = inverse_control(new1_x, new1_y, new1_z)
        joints_robot[6:9] = inverse_control(new3_x, new3_y, new3_z)

        if (t == t_start+1.0):
            phase = 0
            body = True

    joints = joints_robot
    return joints

if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, t = args.m, args.x, args.y, args.t
    
    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de demonstration...')
    elif mode == 'leg_ik':
        print('Mode de controle inverse d\'une patte...')
        (x_def, y_def, z_def) = robot_ref(0, 0, math.pi/4.0, 3*math.pi/4.0)
        (x_def, y_def, z_def) = leg_ref(0, x_def, y_def, z_def)

        x_target = p.addUserDebugParameter("x", -19.7, 19.7, x_def)
        y_target = p.addUserDebugParameter("y", -19.7, 19.7, y_def)
        z_target = p.addUserDebugParameter("z", -15.2, 15.2, z_def)
    elif mode == 'robot_ik':
        print('Mode de controle inverse du corps du robot...')

        x_target = p.addUserDebugParameter("x", -8, 8, 0)
        y_target = p.addUserDebugParameter("y", -8, 8, 0)
        z_target = p.addUserDebugParameter("z", -5, 20, 0)
    elif mode == 'walk':
        print('Mode de marche du robot...')

        x_speed = p.addUserDebugParameter("x_speed", -5, 5, 0)
        y_speed = p.addUserDebugParameter("y_speed", -5, 5, 0)
        t_speed = p.addUserDebugParameter("t_speed", -math.pi/2.0, math.pi/2.0, 0)  # No use for now
    else:
        raise Exception('Mode non implemente: %s' % mode)
    
    t = 0

    # Boucle principale
    while True:
        t += dt

        if mode == 'demo':
            # Recuperation des positions cibles
            joints = demo(t, p.readUserDebugParameter(amplitude))
        elif mode == 'leg_ik':
            x = p.readUserDebugParameter(x_target)
            y = p.readUserDebugParameter(y_target)
            z = p.readUserDebugParameter(z_target)
            joints = leg_ik(x ,y, z, t)
        elif mode == 'robot_ik':
            x = p.readUserDebugParameter(x_target)
            y = p.readUserDebugParameter(y_target)
            z = p.readUserDebugParameter(z_target)
            joints = robot_ik(x ,y, z, t)
        elif mode == 'walk':
            x = p.readUserDebugParameter(x_speed)
            y = p.readUserDebugParameter(y_speed)
            r = p.readUserDebugParameter(t_speed)
            joints = walk(x ,y, r, t)

        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)

        # Mise a jour de la simulation
        p.stepSimulation()
        sleep(dt)
