# Quadruped

Dépendances à installer:

    pip install numpy scipy pybullet jupyter matplotlib

Ensuite:

    python quadruped.py -m mode [-x x-cible -y y-cible -t t-cible]

# Travail réalisé

Leg_ik: Finis
Robot_ik: Finis
Walk: Marche selon x ou y, problème de déviation au cours du temps. La rotation n'a pas été rajouté.
Goto: Non fait
Fun: Non fait
